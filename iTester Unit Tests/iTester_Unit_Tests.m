//
//  iTester_Unit_Tests.m
//  iTester Unit Tests
//
//  Created by Ru Cindrea on 9/30/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@interface iTester_Unit_Tests : SenTestCase

@end

@implementation iTester_Unit_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    STAssertEquals(1, 1, @"just a magic pass here.");
}

@end

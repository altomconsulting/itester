
Given /^I login with "(.*?)" to "(.*?)"$/ do |arg1, arg2|
    if element_exists("button marked:'More info'")
        sleep(1)
        else
        steps %{
            When I press the "Add Self-Hosted Blog" button
            Then I should see "WordPress username"
            And I should see "WordPress password"
            Then I use the native keyboard to enter "#{arg2}" into text field number 1
            Then I use the native keyboard to enter "#{arg1}" into text field number 2
            Then I use the native keyboard to enter "m0b1l3" into text field number 3
            Then I press "Save"
            Then I wait to see "Posts"
            And I take screenshot
        }
    end
end

When /^I select the "(.*?)" post$/ do |arg1|
    steps %{
        Then I wait to see "#{arg1}"
        Then I wait
        Then I touch "#{arg1}"
        And I take screenshot
    }
end

Then /^I should be able to edit it$/ do
    steps %{
        Then I should see a "Update" button
        And I should see "What is the Test Lab?"
        And I should see "This is a placeholder post to explain the test lab concept."
        Then I use the native keyboard to enter " Updated" into text field number 2
        Then I press "Update"
        Then I wait to see "What is the Test Lab? Updated"
        Then I take screenshot
    }
end


And /^I change everything back$/ do
    steps %{
        Then I wait to see "What is the Test Lab? Updated"
        Then I wait until I don't see "Uploading"
        Then I select the "What is the Test Lab? Updated" post
        Then I clear text field number 2
        Then I use the native keyboard to enter "What is the Test Lab?" into text field number 2
        Then I should see a "Update" button
        Then I press "Update"
        Then I wait to see "What is the Test Lab?"
        Then I wait until I don't see "Uploading"
    }
end
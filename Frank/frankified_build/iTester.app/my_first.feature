Feature: Editing a post on a self-hosted blog
   As a Wordpress user with a self-hosted blog
   I want to be able to edit my posts
   So I can easily manage my blog from my phone

Scenario: Edit a post as author
   Given I login with "mobileapp" to "http://thesoftwaretestlab.org"
   When I select the "What is the Test Lab?" post
   Then I should be able to edit it
   And I change everything back


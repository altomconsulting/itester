Feature:
    As an iTester user
    I want to have a way to manage session files
    So I can easily delete or save them

Scenario: Try to upload with no sesssions selected
    Given I launch the app
    When I touch "Organize"
    And I touch "Upload"
    Then I should see an alert view titled "Error uploading sessions"
    Then I touch "Dismiss"
    And I should not see an alert view
    
Scenario: Try to upload and delete with no sessions selected
    Given I launch the app
    When I touch "Organize"
    And I touch "Upload & Delete"
    Then I should see an alert view titled "Error uploading sessions"
    Then I touch "Dismiss"
    And I should not see an alert view
    
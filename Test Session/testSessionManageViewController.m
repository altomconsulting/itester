//
//  testSessionManageViewController.m
//  Test Session
//
//  Created by Ru Cindrea on 3/26/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//
//

#import "testSessionManageViewController.h"
//#import "GTLDrive.h"
//#import "GTMOAuth2ViewControllerTouch.h"
#import "testSessionUtil.h"
#import <QuartzCore/QuartzCore.h>
//#import "CorePlot-CocoaTouch.h"

static NSString* documents[] =
{   @"Text Document.txt",
    @"Image Document.jpg",
    @"PDF Document.pdf",
    @"HTML Document.html"
};

#define NUM_DOCS 4
#define kRowHeight 58.0f

#pragma mark -

@interface testSessionManageViewController ()
//@property (weak, readonly) GTLServiceDrive *driveService;

@property (retain) NSMutableArray *driveFiles;
@property BOOL isAuthorized;
@property NSString *currentLocalFile;
//@property GTLDriveFile *iTesterFolder;
@property NSString *username;
@property NSString *logoutConfirmation;

@property (nonatomic, strong) DirectoryWatcher *docWatcher;
@property (nonatomic, strong) NSMutableArray *documentURLs;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;


//- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
//      finishedWithAuth:(GTMOAuth2Authentication *)auth
//                 error:(NSError *)error;
//- (void)isAuthorizedWithAuthentication:(GTMOAuth2Authentication *)auth;
//- (void)loadDriveFiles;

@end

@implementation testSessionManageViewController

-(BOOL)prefersStatusBarHidden
{
    return NO;
}



@synthesize driveFiles = _driveFiles;
@synthesize isAuthorized = _isAuthorized;
@synthesize tableView;
@synthesize tableData;


- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // start monitoring the document directory…
    NSString *documentsDirectory = [NSHomeDirectory()
                                    stringByAppendingPathComponent:@"Documents"];
    self.docWatcher = [DirectoryWatcher watchFolderWithPath:documentsDirectory delegate:self];
    
    self.documentURLs = [NSMutableArray array];
    
    // scan for existing documents
    [self directoryDidChange:self.docWatcher];
}

- (void)viewDidUnload
{
    self.documentURLs = nil;
    self.docWatcher = nil;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.documentURLs.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title =  @"Local Sessions";
    return title;
}

- (UITableViewCell *)tableView:(UITableView*)table cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellID";
    UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    NSURL *fileURL = [self.documentURLs objectAtIndex:indexPath.row];
	[self setupDocumentControllerWithURL:fileURL];
	
    // layout the cell
    cell.textLabel.text = [[fileURL path] lastPathComponent];
    NSInteger iconCount = [self.docInteractionController.icons count];
    if (iconCount > 0)
    {
        cell.imageView.image = [self.docInteractionController.icons objectAtIndex:iconCount - 1];
    }
    
    NSString *fileURLString = [self.docInteractionController.URL path];
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURLString error:nil];
    NSInteger fileSize = [[fileAttributes objectForKey:NSFileSize] intValue];
    NSString *fileSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize
                                                           countStyle:NSByteCountFormatterCountStyleFile];
    NSString *fileDate = [fileAttributes objectForKey:NSFileModificationDate];
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", fileSizeStr, self.docInteractionController.UTI];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - .csv - %@", fileSizeStr, fileDate];
    cell.imageView.userInteractionEnabled = YES;    // this is by default NO, so we need to turn it on
    
    return cell;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kRowHeight;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSURL *fileURL;
     fileURL = [self.documentURLs objectAtIndex:indexPath.row];
    
     [self setupDocumentControllerWithURL:fileURL];
     [self.docInteractionController presentPreviewAnimated:YES];
}


#pragma mark - UIDocumentInteractionControllerDelegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController
{
    return self;
}


#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    NSInteger numToPreview = 0;
    
    numToPreview = self.documentURLs.count;
    return numToPreview;
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    NSURL *fileURL = nil;
    
    fileURL = [self.documentURLs objectAtIndex:idx];
    
    return fileURL;
}


#pragma mark - File system support

- (NSString *)applicationDocumentsDirectory
{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
	[self.documentURLs removeAllObjects];    // clear out the old docs and start over
	
	NSString *documentsDirectoryPath = [self applicationDocumentsDirectory];
	
	NSArray *documentsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectoryPath
                                                                                              error:NULL];
    
	for (NSString* curFileName in [documentsDirectoryContents objectEnumerator])
	{
		NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:curFileName];
		NSURL *fileURL = [NSURL fileURLWithPath:filePath];
		
		BOOL isDirectory;
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
		
        // proceed to add the document URL to our list (ignore the "Inbox" folder)
        if (!(isDirectory && [curFileName isEqualToString:@"Inbox"]))
        {
            [self.documentURLs addObject:fileURL];
        }
	}
	
	[self.tableView reloadData];
}


- (IBAction)done:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
}

- (void)tableView:(UITableView *)table commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *selectedCell = [table cellForRowAtIndexPath:indexPath];
    NSError *error;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *filename = selectedCell.textLabel.text;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDir = [paths objectAtIndex:0];
        NSString *fullFilePath = [documentDir stringByAppendingPathComponent:filename];
        [fileMgr removeItemAtPath:fullFilePath error:&error];
        if (error) {
            NSLog(@"error");
        }
        [tableData removeObjectAtIndex:indexPath.row];
    }
}

@end

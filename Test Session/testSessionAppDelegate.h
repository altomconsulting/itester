//
//  testSessionAppDelegate.h
//  Test Session
//
//  Created by Ru Cindrea on 3/18/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface testSessionAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

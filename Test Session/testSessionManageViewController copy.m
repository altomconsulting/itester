//
//  testSessionManageViewController.m
//  Test Session
//
//  Created by Ru Cindrea on 3/26/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//
// test

#import "testSessionManageViewController.h"
//#import "GTLDrive.h"
//#import "GTMOAuth2ViewControllerTouch.h"
#import "testSessionUtil.h"
#import <QuartzCore/QuartzCore.h>
#import "CorePlot-CocoaTouch.h"

static NSString *const kKeychainItemName = @"iTester";
static NSString *const kClientID = @"712556608618.apps.googleusercontent.com";
static NSString *const kClientSecret = @"2BUkgfSJjZUh-bwmuwSh3m8U";


@interface testSessionManageViewController ()
//@property (weak, readonly) GTLServiceDrive *driveService;

@property (retain) NSMutableArray *driveFiles;
@property BOOL isAuthorized;
@property NSString *currentLocalFile;
//@property GTLDriveFile *iTesterFolder;
@property NSString *username;
@property NSString *logoutConfirmation;

@property (nonatomic, strong) NSMutableArray *documentURLs;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;

- (IBAction)uploadAndDelete:(id)sender;

- (IBAction)authButtonClicked:(id)sender;

//- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
//      finishedWithAuth:(GTMOAuth2Authentication *)auth
//                 error:(NSError *)error;
//- (void)isAuthorizedWithAuthentication:(GTMOAuth2Authentication *)auth;
//- (void)loadDriveFiles;

@end

@implementation testSessionManageViewController

-(BOOL)prefersStatusBarHidden
{
    return YES;
}



@synthesize authButton = _authButton;
@synthesize driveFiles = _driveFiles;
@synthesize isAuthorized = _isAuthorized;
@synthesize sessionTableView;
@synthesize tableData;


@synthesize offlineView;
@synthesize offlineSessions;

NSMutableArray *selectedSessions;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    tableData = [[NSMutableArray alloc] init];
    selectedSessions = [[NSMutableArray alloc] init];
    
    sessionTableView.delegate = self;
    sessionTableView.dataSource = self;
    
//    self.driveFile = [[GTLDriveFile alloc] init];

    [self loadLocalFiles];
//    GTMOAuth2Authentication *auth =
//    [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
//                                                          clientID:kClientID
//                                                      clientSecret:kClientSecret];
//    if ([auth canAuthorize]) {
//        [self isAuthorizedWithAuthentication:auth];
//        self.authButton.title = auth.userEmail;
//    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setOfflineView:nil];
    [self setOfflineView:nil];
    [self setAuthButton:nil];
    [self setUploadButton:nil];
    [self setUploadButton:nil];
    [self setShowSessionsButton:nil];
    [super viewDidUnload];
    
}

#pragma mark Table Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];

    NSString *file = [tableData objectAtIndex:indexPath.row];
    cell.textLabel.text = file;
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    QLPreviewController *previewController = [[QLPreviewController alloc] init];
    previewController.dataSource = self;
    previewController.delegate = self;
    
    // start previewing the document at the current section index
    previewController.currentPreviewItemIndex = indexPath.row;
    [[self navigationController] pushViewController:previewController animated:YES];
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    NSURL *fileURL = nil;
    
    NSIndexPath *selectedIndexPath = [self.sessionTableView indexPathForSelectedRow];
    if (selectedIndexPath.section == 0)
    {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        UITableViewCell *selectedCell = [sessionTableView cellForRowAtIndexPath:selectedIndexPath];
        NSString *filename = selectedCell.textLabel.text;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDir = [paths objectAtIndex:0];
        NSString *fullFilePath = [documentDir stringByAppendingPathComponent:filename];
        fileURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:fullFilePath ofType:nil]];
    }
    else
    {
        fileURL = [self.documentURLs objectAtIndex:idx];
    }
    
    return fileURL;
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    
//    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
//    
//    if ([selectedCell accessoryType] == UITableViewCellAccessoryNone) {
//        [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
//
//        [selectedSessions addObject:selectedCell.textLabel.text];
//
//    } else {
//        [selectedCell setAccessoryType:UITableViewCellAccessoryNone];
//        [selectedSessions removeObjectIdenticalTo:selectedCell.textLabel.text];
//    }
//        
//    [tableView deselectRowAtIndexPath:indexPath animated:NO];
//    
//}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSError *error;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *filename = selectedCell.textLabel.text;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDir = [paths objectAtIndex:0];
        NSString *fullFilePath = [documentDir stringByAppendingPathComponent:filename];
        [fileMgr removeItemAtPath:fullFilePath error:&error];
        if (error) {
            NSLog(@"error");
        }
        [tableData removeObjectAtIndex:indexPath.row];
    }
[tableView reloadData];
}

//- (NSInteger)didUpdateFileWithIndex:(NSInteger)index
//                          driveFile:(GTLDriveFile *)driveFile {
//    if (index == -1) {
//        if (driveFile != nil) {
//            // New file inserted.
//            [self.driveFiles insertObject:driveFile atIndex:0];
//            index = 0;
//        }
//    } else {
//        if (driveFile != nil) {
//            // File has been updated.
//            [self.driveFiles replaceObjectAtIndex:index withObject:driveFile];
//        } else {
//            // File has been deleted.
//            [self.driveFiles removeObjectAtIndex:index];
//            index = -1;
//        }
//    }
//    return index;
//}
//
//- (GTLServiceDrive *)driveService {
//    static GTLServiceDrive *service = nil;
//    
//    if (!service) {
//        service = [[GTLServiceDrive alloc] init];
//        
//        service.shouldFetchNextPages = YES;
//        service.retryEnabled = YES;
//    }
//    return service;
//}

- (IBAction)uploadAndDelete:(id)sender {
    if (self.isAuthorized) {
    //[self uploadSessions:@"yes"];
    }
    
    else {
        [testSessionUtil showErrorMessageWithTitle:@"Error uploading sessions" message:@"Sign in to Google Drive first to upload your sessions." delegate:self];
    }
    
}
    

- (IBAction)authButtonClicked:(id)sender {
    if (!self.isAuthorized) {
        // Sign in.
        
                
        SEL finishedSelector = @selector(viewController:finishedWithAuth:error:);

//        GTMOAuth2ViewControllerTouch *authViewController =
//        [[GTMOAuth2ViewControllerTouch alloc] initWithScope:kGTLAuthScopeDriveFile
//                                                   clientID:kClientID
//                                               clientSecret:kClientSecret
//                                           keychainItemName:kKeychainItemName
//                                                   delegate:self
//                                           finishedSelector:finishedSelector];
    
        
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:authViewController];
        
        
        //navController.title = @"Google Drive";
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(viewController:finishedWithAuth:error:)];
        
        self.navigationController.navigationItem.hidesBackButton = NO;
        
//        navController.navigationBar.tintColor = [UIColor blackColor];
//        navController.navigationBar.topItem.leftBarButtonItem = cancelButton;
//        navController.navigationBar.topItem.rightBarButtonItem = nil;
//        [self presentViewController:navController animated:YES completion:nil];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.authButton.title
                                                        message:@"Are you sure you want to log out?"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Log out", nil];
        
        [alert show];
    }
}

//- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
//      finishedWithAuth:(GTMOAuth2Authentication *)auth
//                 error:(NSError *)error {
//    [self dismissViewControllerAnimated:YES completion:nil];
//    if (error == nil) {
//        [self isAuthorizedWithAuthentication:auth];
//    }
//}

//- (void)isAuthorizedWithAuthentication:(GTMOAuth2Authentication *)auth {
 //   [[self driveService] setAuthorizer:auth];
 //   self.authButton.title = auth.userEmail;
 //   self.isAuthorized = YES;
    //[self loadDriveFiles];
//}


- (void)loadLocalFiles {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory()
                                    stringByAppendingPathComponent:@"Documents"];
    NSDirectoryEnumerator *dirEnum = [fileMgr enumeratorAtPath:documentsDirectory];
    
    NSString *file;
    while (file = [dirEnum nextObject]) {
        if ([[file pathExtension] isEqualToString: @"csv"]) {
            [tableData addObject:file];
        }
    }
}

//- (void)loadDriveFiles {
//    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
//    query.q = @"mimeType = 'text/csv'";
//    
//    UIAlertView *alert = [testSessionUtil showLoadingMessageWithTitle:@"Loading files"
//                                                             delegate:self];
//    [self.driveService executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
//                                                              GTLDriveFileList *files,
//                                                              NSError *error) {
//        [alert dismissWithClickedButtonIndex:0 animated:YES];
//        if (error == nil) {
//            if (self.driveFiles == nil) {
//                self.driveFiles = [[NSMutableArray alloc] init];
//            }
//            [self.driveFiles removeAllObjects];
//            [self.driveFiles addObjectsFromArray:files.items];
//            //[self.tableView reloadData];
//        } else {
//            NSLog(@"An error occurred: %@", error);
//            [testSessionUtil showErrorMessageWithTitle:@"Unable to load files"
//                                               message:[error description]
//                                             delegate:self];
//        }
//    }];
//}

- (IBAction)done:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
                                
}
- (IBAction)upload:(id)sender {
    [self showActivityViewController];
    
}

//-(void)uploadSessions:(NSString *) delete {
//    GTLUploadParameters *uploadParameters = nil;
//    
//    if (selectedSessions.count == 0) {
//        [testSessionUtil showErrorMessageWithTitle:@"Error uploading sessions" message:@"You need to select some sessions first." delegate:self];
//    }
//    else {
//    
//    for (NSString *filename in selectedSessions) {
//        
//        
//        NSFileManager *fileMgr = [NSFileManager defaultManager];
//
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentDir = [paths objectAtIndex:0];
//        NSString *fullFilePath = [documentDir stringByAppendingPathComponent:filename];
//        
//        NSString *textFromFile = [NSString stringWithContentsOfFile:fullFilePath encoding:NSUTF8StringEncoding error:nil];
//        
//        NSData *fileContent =
//        [textFromFile dataUsingEncoding:NSUTF8StringEncoding];
//        uploadParameters =
//        [GTLUploadParameters uploadParametersWithData:fileContent MIMEType:@"text/plain"];
//        
//        self.driveFile.title = filename;
//        
//        GTLQueryDrive *query = nil;
//        if (self.driveFile.identifier == nil || self.driveFile.identifier.length == 0) {
//            // This is a new file, instantiate an insert query.
//            query = [GTLQueryDrive queryForFilesInsertWithObject:self.driveFile
//                                                uploadParameters:uploadParameters];
//        } else {
//            //This file already exists, instantiate an update query.
//            query = [GTLQueryDrive queryForFilesUpdateWithObject:self.driveFile
//                                                          fileId:self.driveFile.identifier
//                                                uploadParameters:uploadParameters];
//        }
//        
//        
//        UIAlertView *alert = [testSessionUtil showLoadingMessageWithTitle:filename delegate:self];
//        [self.driveService executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
//                                                                  GTLDriveFile *updatedFile,
//                                                                  NSError *error) {
//            
//            [alert dismissWithClickedButtonIndex:0 animated:YES];
//            
//            if (error == nil) {
//                self.driveFile = updatedFile;
//            } else {
//                NSLog(@"An error occurred: %@", error);
//                [testSessionUtil showErrorMessageWithTitle:@"Unable to save file"
//                                                   message:[error description]
//                                                  delegate:self];
//            }
//            
//            if ([delete isEqual:@"yes"]) {
//                [fileMgr removeItemAtPath:fullFilePath error:&error];
//                if (error) {
//                    [testSessionUtil showErrorMessageWithTitle:@"Unable to save file"
//                                                       message:[error description]
//                                                      delegate:self];                }
//                NSUInteger index = [self.tableData indexOfObject:filename];
//
//                [self.tableData removeObjectAtIndex:index];
//
//            }
//         [self.sessionTableView reloadData];
//
//        }];
//    }
//}
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
 
        if([title isEqualToString:@"Log out"])
        {
//            [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
            //[[self driveService] setAuthorizer:nil];
            self.authButton.title = @"Sign in";
            self.isAuthorized = NO;
            NSLog(@"User signed out.");
            [self.driveFiles removeAllObjects];
        
        }
}

-(void)showActivityViewController
{
    NSArray *activityItems;
    
    if (selectedSessions.count == 0) {
            [testSessionUtil showErrorMessageWithTitle:@"Error uploading sessions" message:@"You need to select some sessions first." delegate:self];
        }
    else {
        
        for (NSString *filename in selectedSessions) {

            NSFileManager *fileMgr = [NSFileManager defaultManager];
    
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentDir = [paths objectAtIndex:0];
            NSString *fullFilePath = [documentDir stringByAppendingPathComponent:filename];
    
            NSString *textFromFile = [NSString stringWithContentsOfFile:fullFilePath encoding:NSUTF8StringEncoding error:nil];
    
            NSData *fileContent =
            [textFromFile dataUsingEncoding:NSUTF8StringEncoding];
            //uploadParameters =
            //[GTLUploadParameters uploadParametersWithData:fileContent MIMEType:@"text/plain"]
            activityItems = [NSArray arrayWithObjects:fileContent, nil];
        }
    
    
    //-- initialising the activity view controller
    UIActivityViewController *avc = [[UIActivityViewController alloc]
                                     initWithActivityItems:activityItems
                                     applicationActivities:nil];
    
    //-- define the activity view completion handler
    avc.completionHandler = ^(NSString *activityType, BOOL completed){
        NSLog(@"Activity Type selected: %@", activityType);
        if (completed) {
            NSLog(@"Selected activity was performed.");
        }
        else
        {
            if (activityType == NULL) {
                NSLog(@"User dismissed the view controller without making a selection.");
            } else {
                NSLog(@"Activity was not performed.");
            }
        }
    };
    
    //-- define activity to be excluded (if any)
    avc.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypeAssignToContact, nil];
    
    //-- show the activity view controller
    [self presentViewController:avc
                       animated:YES completion:nil];

    }
}
@end


//
//  testSessionViewController.h
//  Test Session
//
//  Created by Ru Cindrea on 3/18/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "CMPopTipView.h"

@interface testSessionViewController : UIViewController  <UITextViewDelegate, MFMailComposeViewControllerDelegate, CMPopTipViewDelegate> {
    UILabel *timerLabel;
    NSTimer *pollingTimer;
    NSDateFormatter *dateFormatter;
    int sessionStart;
}


@property (nonatomic, strong)	NSMutableArray	*visiblePopTipViews;
@property (nonatomic, strong)	id				currentPopTipViewTarget;


@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (nonatomic, assign) int timerOn;
@property (nonatomic, assign) int timeOnPause;
@property (nonatomic, assign) int sessionStart;
@property (strong, nonatomic) IBOutlet UIProgressView *progress;
@property (strong, nonatomic) IBOutlet UISegmentedControl *activityControl;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (strong, nonatomic) NSString *reporter;
@property (strong, nonatomic) NSString *environment;
@property (strong, nonatomic) NSString *area;

@property (nonatomic) float remainingTime;
@property (strong, nonatomic) NSDictionary *fontAttributes;

@property (strong, nonatomic) IBOutlet UISegmentedControl *nonTimeActivityControl;
@property (strong, nonatomic) IBOutlet UILabel *activeTimeLabel;
- (IBAction)nonTimeActivityChanged:(id)sender;

- (IBAction)activityChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UILabel *charterLabel;
@property (nonatomic) NSString *charter;
@property (strong, nonatomic) NSString *sessionFile;
@property (strong, nonatomic) NSString *sessionName;
@property (nonatomic) float minutes;
@property (nonatomic) float timePassed;

@property (strong, nonatomic) IBOutlet UITextView *activityDescription;

@property (strong, nonatomic) IBOutlet UITextView *history;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *pauseSendButton;

@property (nonatomic) int numberBugs;
@property (nonatomic) int numberQuestions;
@property (nonatomic) int numberNextTime;
@property (nonatomic) float timeOnBugs;
@property (nonatomic) float timeOnSetup;
@property (nonatomic) float timeOffCharter;
@property (nonatomic) float timeOnTest;
@property (strong, nonatomic) CMPopTipView *activityChangedTip;
@property (strong, nonatomic) CMPopTipView *help;

@property (nonatomic) NSString *currentActivity;


- (IBAction)saveActivity:(id)sender;
- (IBAction)pauseSendSession:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;
- (void) pollTime;
@end

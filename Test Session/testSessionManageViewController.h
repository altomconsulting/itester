//
//  testSessionManageViewController.h
//  Test Session
//
//  Created by Ru Cindrea on 3/26/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>
#import "DirectoryWatcher.h"

@interface testSessionManageViewController : UIViewController <QLPreviewControllerDataSource,QLPreviewControllerDelegate, DirectoryWatcherDelegate, UIDocumentInteractionControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)done:(id)sender;

@property NSMutableArray *tableData;

@end

//
//  testSessionHelp.h
//  Test Session
//
//  Created by Ru Cindrea on 4/20/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@interface testSessionHelp : UIViewController <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextView *helpTextView;

- (IBAction)donePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *feedbackButton;

- (IBAction)feedback:(id)sender;

@end

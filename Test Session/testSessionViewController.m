//
//  testSessionViewController.m
//  Test Session
//
//  Created by Ru Cindrea on 3/18/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import "testSessionViewController.h"
#define kPollingInterval 1
#define defaultTextActivity @"Tap here to enter text. If you have Siri enabled, you can use the microphone to record speech."
#import <QuartzCore/QuartzCore.h>
#import "CMPopTipView.h"


@implementation testSessionViewController


-(BOOL)prefersStatusBarHidden
{
    return NO;
}

@synthesize timerLabel;
@synthesize dateFormatter;
@synthesize minutes;
@synthesize charter;
@synthesize sessionStart;
@synthesize activityDescription;
@synthesize activityControl;
@synthesize sessionFile;
@synthesize history;
@synthesize doneButton;
@synthesize reporter;
@synthesize area;
@synthesize environment;



- (void) pollTime
{   
    if (self.timerOn == 0)
    {
        self.timeOnPause = (self.timeOnPause + 1);
    }
    
    
    NSDate *now = [[NSDate alloc] init];
    int timeNow = [now timeIntervalSince1970];
    
    _remainingTime = minutes*60 + self.timeOnPause - (timeNow - self.sessionStart);
    

    
    if ([self.activeTimeLabel.text isEqualToString:@"Time on Test active"]){
        self.timeOnTest = self.timeOnTest + (timeNow - self.sessionStart - self.timePassed);
        //NSLog(@"Time on Test: %2.1d", self.timeOnTest);

    }
    else if ([self.activeTimeLabel.text isEqualToString:@"Time on Bugs active"]){
        self.timeOnBugs = self.timeOnBugs + (timeNow -self.sessionStart - self.timePassed);
    }
    else if ([self.activeTimeLabel.text isEqualToString:@"Time on Setup active"]){
        self.timeOnSetup = self.timeOnSetup + (timeNow -self.sessionStart - self.timePassed);
    }
    else if ([self.activeTimeLabel.text isEqualToString:@"Time Off Charter active"]){
        self.timeOffCharter = self.timeOffCharter + (timeNow -self.sessionStart - self.timePassed);
    }

    int mins = floor(_remainingTime/60);
    int secs = trunc(_remainingTime - mins * 60);
    
    self.timerLabel.text = [NSString stringWithFormat:@"%2.0d mins %2.0d secs",mins, secs];
    self.progress.progress = (minutes*60-_remainingTime)/(minutes*60);
    
    if (_remainingTime < 0) {
        self.timerOn = 0;
        self.progress.progress = 1;
        self.timerLabel.text = @"out of time";
    }
    
    self.timePassed = timeNow - self.sessionStart;
    //NSLog(@"Time passed: %2.1f", self.timePassed);
}

- (void)configurePopUpTipView: (NSString*) text :(UIView*) pointAtView :(PointDirection) direction{
    
    CMPopTipView *popTipView;

    [popTipView dismissAnimated:YES];
    popTipView = [[CMPopTipView alloc] initWithMessage:text];
    
    popTipView.backgroundColor = [UIColor whiteColor];
    //popTipView.textColor = [UIColor colorWithRed:77.0/256.0 green:168.0/256.0 blue:36.0/256.0 alpha:1.0];
    popTipView.textColor = [UIColor blackColor];
    popTipView.borderColor = [UIColor colorWithRed:77.0/256.0 green:168.0/256.0 blue:36.0/256.0 alpha:1.0];
    popTipView.textFont = [UIFont fontWithName:@"Optima" size:13.0];
    popTipView.preferredPointDirection = direction;
    [popTipView presentPointingAtView:pointAtView inView:self.view animated:YES];
    [popTipView autoDismissAnimated:YES atTimeInterval:20.0];
    [popTipView dismissTapAnywhere];
    [self.visiblePopTipViews addObject:popTipView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _remainingTime = minutes*60;
    
    self.timePassed = 0;
    self.numberBugs = 0;
    self.timeOnBugs = 0;
    self.timeOnSetup = 0;
    self.timeOffCharter = 0;
    
    self.currentActivity = @"Test";
    self.visiblePopTipViews = [NSMutableArray array];

    
    if ([self.area isEqualToString:@""]) {
        self.area = @"General";
    }
    
    if ([self.environment isEqualToString:@""]) {
        self.environment = @"N/A";
    }
    
    [self.activityDescription.layer setBorderColor: [[UIColor lightGrayColor] CGColor]];
    [self.activityDescription.layer setBorderWidth: 0.5];
    [self.activityDescription.layer setCornerRadius:8.0f];
    [self.activityDescription.layer setMasksToBounds:YES];
    
    [self.activityControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [self.nonTimeActivityControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [self.nonTimeActivityControl setTintColor:[UIColor lightGrayColor]];
    
    // Here you get access to the file in Documents directory of your application bundle.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [paths objectAtIndex:0];
    NSDate *date = [[NSDate alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd-HH:mm:ss"];
    
    NSMutableString *charterFiltered = [NSMutableString stringWithString:[charter stringByReplacingOccurrencesOfString:@"," withString:@""]];
    charterFiltered = [NSMutableString stringWithString:[charterFiltered stringByReplacingOccurrencesOfString:@" " withString:@"_"]];

    
    self.sessionName = [NSString stringWithFormat:@"Charter_%@_%@.csv", charterFiltered, [dateFormatter stringFromDate:date]];
    sessionFile = [documentDir stringByAppendingPathComponent:self.sessionName];
    NSString *timestamp = [dateFormatter stringFromDate:date];
    
    NSString *stringToSave = [NSString stringWithFormat:@"%@,Session Tester,%@\n%@,Session Charter,%@\n%@,Planned session time (mins),%2.0f\n%@, Environment Info, %@\n%@, Area, %@\n", timestamp, self.reporter,timestamp, charter, timestamp, minutes, timestamp, self.environment, timestamp, self.area];
    
    float notificationTime;
    NSString *notificationText;
    
    NSDate *now = [[NSDate alloc] init];
    if (minutes < 6) {
        notificationTime = minutes*60 - 3*60;
        notificationText = @"iTester Session - 3 minutes left!";
        NSDate *notificationDate = [now dateByAddingTimeInterval:notificationTime];
        [self createNotification:notificationDate :notificationText];
    }
    else {
        notificationTime = minutes*60 - 60*5;
        notificationText = @"iTester Session - 5 minutes left!";
        NSDate *notificationDate = [now dateByAddingTimeInterval:notificationTime];
        [self createNotification:notificationDate :notificationText];
    }
    

    NSString *textFromFile = [NSString stringWithContentsOfFile:sessionFile encoding:NSUTF8StringEncoding error:nil];
    
    if(textFromFile){
        NSString *textToFile = [textFromFile stringByAppendingString:[NSString stringWithFormat:@"%@",stringToSave]];
        [textToFile writeToFile:sessionFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
    }
    else{
        [stringToSave writeToFile:sessionFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    textFromFile = [NSString stringWithContentsOfFile:sessionFile encoding:NSUTF8StringEncoding error:nil];
    [history setText:[self formatHistory:textFromFile]];


    NSDate *today = [[NSDate alloc] init];
    self.sessionStart = [today timeIntervalSince1970];
    self.progress.progress = 0;
    activityDescription.delegate = self;

    
    _timeOnPause = 0;
    _timerOn = 1;
    self.timerLabel.text = @"time left..";
    pollingTimer = [NSTimer scheduledTimerWithTimeInterval:kPollingInterval
                                                    target:self
                                                    selector:@selector(pollTime)
                                                    userInfo:nil
                                                    repeats:YES];
    
    _fontAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor],
                        NSFontAttributeName: [UIFont fontWithName:@"Optima" size:14.0]//,
                        // NSShadowAttributeName: textShadow
                        };
    
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:charter attributes:_fontAttributes];
    self.charterLabel.attributedText = string;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView {
    [self.visiblePopTipViews removeObject:popTipView];
    self.currentPopTipViewTarget = nil;
}


- (void)dismissAllPopTipViews {
	while ([self.visiblePopTipViews count] > 0) {
		CMPopTipView *popTipView = [self.visiblePopTipViews objectAtIndex:0];
		[popTipView dismissAnimated:YES];
		[self.visiblePopTipViews removeObjectAtIndex:0];
    }
}

- (IBAction)pauseSendSession:(id)sender {
    

    if ([self.pauseSendButton.title isEqual:@"Email"]) {
        NSString *emailTitle = @"iTester Session";
        NSString *messageBody = [NSString stringWithFormat:@"Test Charter: %@\niTester session attached.", charter];
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:self.reporter];
        NSData *sessionData = [NSData dataWithContentsOfFile:sessionFile];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        [mc addAttachmentData:sessionData mimeType:@"text/csv" fileName:self.sessionName];
        
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else if ([self.pauseSendButton.title isEqual:@"Help"]){
        
        [self dismissAllPopTipViews];
        [self configurePopUpTipView:@"These note types will affect time calculations. Time on Bugs, Time on Test, Time on Setup and Time Off Charter will be calculated for each session." :self.activityControl :PointDirectionUp];
        
        self.currentPopTipViewTarget = sender;

        [self.pauseSendButton setTitle:@"More 2/5"];

    }
    else if ([self.pauseSendButton.title isEqual:@"More 2/5"]){
        [self dismissAllPopTipViews];
        [self configurePopUpTipView:@"The current active time is shown here" :self.activeTimeLabel :PointDirectionDown];
        [self.pauseSendButton setTitle:@"More 3/5"];
        self.currentPopTipViewTarget = sender;


    }
    
    else if ([self.pauseSendButton.title isEqual:@"More 3/5"]){
        [self dismissAllPopTipViews];

        [self configurePopUpTipView:@"Every time you click on one of these note types, the current active time and all session time calculations will immediately switch to the type you have chosen." :self.activityControl :PointDirectionUp];
        
        [self.pauseSendButton setTitle:@"More 4/5"];
        self.currentPopTipViewTarget = sender;

    }
    
    else if ([self.pauseSendButton.title isEqual:@"More 4/5"]){
        [self dismissAllPopTipViews];

        [self configurePopUpTipView:@"These note types will NOT affect time calculations.\n\nYou can save different notes, questions, ideas for next time or data-related info within Time on Bugs, Time on Test, Time on Setup or Time Off Charter." :self.nonTimeActivityControl :PointDirectionUp];
        [self.pauseSendButton setTitle:@"More 5/5"];
        self.currentPopTipViewTarget = sender;

    }
    else if ([self.pauseSendButton.title isEqual:@"More 5/5"]){
        [self dismissAllPopTipViews];
        [self configurePopUpTipView:@"You can see the actual session content and a history of all your notes here. The session will be saved in .csv format. Each entry/row will have a timestamp, a note type and the note itself. \n\nWhen finishing a test session, the total number of bugs, questions and next time ideas as well as the total time spent on bugs, tests, setup or off charter will be saved in the beginning of the file." :self.history :PointDirectionDown];
        
        [self.pauseSendButton setTitle:@"Help"];
        self.currentPopTipViewTarget = sender;

    }

}

- (void) createNotification:(NSDate*)dateString :(NSString*)textString
{
	NSLog(@"createNotification");
    
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    if (notification)
    {
        notification.fireDate = dateString;
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.repeatInterval = 0;
		notification.alertBody = textString;
        [app scheduleLocalNotification:notification];
    }	
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    //self.currentActivity = [self.activityControl titleForSegmentAtIndex:self.activityControl.selectedSegmentIndex];
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)doneButtonPressed:(id)sender {
    
    self.timerOn = 0;
    
    if (![doneButton.title isEqual: @"Close"]) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        
        NSDate *date = [[NSDate alloc] init];
        dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"YYYY-MM-dd-HH:mm:ss"];
        NSString *timestamp = [dateFormatter stringFromDate:date];
        
        float duration = minutes - _remainingTime/60;
        NSString *stringToSave = [NSString stringWithFormat:@"%@,Session end.Duration,%2.1f\n%@,Number of bugs,%i\n%@,Time on Test (mins),%2.1f\n%@,Time on Setup (mins),%2.1f\n%@,Time on Bugs (mins),%2.1f\n%@,Time Off Charter (mins),%2.1f\n%@,Number of questions,%i\n%@,Number of ideas for next time,%i\n", timestamp, duration, timestamp, self.numberBugs, timestamp, self.timeOnTest/60, timestamp, self.timeOnSetup/60, timestamp, self.timeOnBugs/60, timestamp, self.timeOffCharter/60, timestamp, self.numberQuestions, timestamp,self.numberNextTime];
        
        NSString *textFromFile = [NSString stringWithContentsOfFile:sessionFile encoding:NSUTF8StringEncoding error:nil];
        
        if(textFromFile){
            NSString *textToFile = [stringToSave stringByAppendingString:[NSString stringWithFormat:@"%@",textFromFile]];
            //NSString *textToFile = [textFromFile stringByAppendingString:[NSString stringWithFormat:@"%@",stringToSave]];
            [textToFile writeToFile:sessionFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
            
        }
        else{
            [stringToSave writeToFile:sessionFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
        }
        
        textFromFile = [NSString stringWithContentsOfFile:sessionFile encoding:NSUTF8StringEncoding error:nil];

        [history setText:[self formatHistory:textFromFile]];

        [doneButton setTitle:@"Close"];
        [self.pauseSendButton setTitle:@"Email"];
        [self.pauseSendButton setTintColor:[UIColor colorWithRed:77.0/256.0 green:168.0/256.0 blue:36.0/256.0 alpha:1.0]];

        
        
        [self.activityDescription setEditable:false];
        [self.saveButton setEnabled:false];
        

        }
    
    else if ([doneButton.title isEqual:@"Close"]) {
        
        [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
    }
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }

    [doneButton setTitle:@"Close"];

    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}



- (IBAction)saveActivity:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self.activityDescription.text isEqual:@""]||[self.activityDescription.text isEqual:defaultTextActivity])
    {
        //Do nothing?
        //[self.activityDescription setText:@"Enter some text first..."];
    }
    else {
    
    
    NSString *activityRaw = [activityDescription text];
    //[activityDescription setText:defaultTextActivity];
    [activityDescription setText:@""];
        
    NSMutableString *activity = [NSMutableString stringWithString:[activityRaw stringByReplacingOccurrencesOfString:@"," withString:@" "]];
    [activity insertString:@"\"" atIndex:0];
    [activity insertString:@"\"" atIndex:activity.length];
    
    
    NSDate *date = [[NSDate alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd-HH:mm:ss"];
    NSString *timestamp = [dateFormatter stringFromDate:date];
    
    if ([self.currentActivity isEqual:@"Bug"]) {
            self.numberBugs ++;
        }
    else if ([self.currentActivity isEqual:@"Question"]) {
        self.numberQuestions ++;
    }
    else if ([self.currentActivity isEqual:@"NextTime"]) {
        self.numberNextTime ++;
    }

    
    
    NSString *stringToSave = [NSString stringWithFormat:@"%@,%@,%@\n", timestamp, self.currentActivity, activity];
    
    NSString *textFromFile = [NSString stringWithContentsOfFile:sessionFile encoding:NSUTF8StringEncoding error:nil];
    
    if(textFromFile){    
        NSString *textToFile = [textFromFile stringByAppendingString:[NSString stringWithFormat:@"%@",stringToSave]];
        [textToFile writeToFile:sessionFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
    }
    else{
        [stringToSave writeToFile:sessionFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    textFromFile = [NSString stringWithContentsOfFile:sessionFile encoding:NSUTF8StringEncoding error:nil];
    
    [history setText:[self formatHistory:textFromFile]];
    
    
}
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqual:defaultTextActivity]){
        [textView setText:@""];
    }
    //use animateTextField function to scroll the view up to show text if needed
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
}

-(void)animateTextField:(UITextView*)textView up:(BOOL)up
{
    const int movementDistance = -120;
    const float movementDuration = 0.3f;
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, movement, 0);
    [UIView commitAnimations];
}

- (NSString*)detectActivity:(NSString *)string {
    NSString * activityType;
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"," withString:@""];
    string = [string lowercaseString];
    
    if ([string rangeOfString:@"newnote"].location != NSNotFound) {
        activityType = @"Note";
    }
    else if ([string rangeOfString:@"newbug"].location != NSNotFound) {
        activityType = @"Bug";
    }
    else if ([string rangeOfString:@"newoffcharter"].location != NSNotFound) {
        activityType = @"Off Charter";
    }

    else if ([string rangeOfString:@"newtest"].location != NSNotFound) {
        activityType = @"Test";
    }

    else if ([string rangeOfString:@"newsetup"].location != NSNotFound) {
        activityType = @"Setup";
    }

    else {
        activityType = @"unknown";
    }

    return activityType;
}

- (NSString*)formatHistory:(NSString *)string {
    
    NSString * formattedString;
    formattedString = [string stringByReplacingOccurrencesOfString:@"," withString:@" - "];
    return formattedString;
}

- (void) clearNotifications {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)viewDidUnload {
    [self setActivityDescription:nil];
    [self setHistory:nil];
    [self setDoneButton:nil];
    [self setSaveButton:nil];
    [self setNonTimeActivityControl:nil];
    [super viewDidUnload];
}
- (IBAction)nonTimeActivityChanged:(id)sender {
    self.currentActivity = [self.nonTimeActivityControl titleForSegmentAtIndex:self.nonTimeActivityControl.selectedSegmentIndex];
    
    for (int i=0; i<[self.nonTimeActivityControl.subviews count]; i++)
    {
        if ([[self.nonTimeActivityControl.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && [[self.nonTimeActivityControl.subviews objectAtIndex:i]isSelected])
        {
            [[self.nonTimeActivityControl.subviews objectAtIndex:i] setTintColor:[UIColor darkGrayColor]];
        }
        if ([[self.nonTimeActivityControl.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && ![[self.nonTimeActivityControl.subviews objectAtIndex:i] isSelected])
        {
            [[self.nonTimeActivityControl.subviews objectAtIndex:i] setTintColor:[UIColor lightGrayColor]];
        }
    }
    
    [self.nonTimeActivityControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [self.activityControl setTintColor:[UIColor colorWithRed:77.0/256.0 green:168.0/256.0 blue:36.0/256.0 alpha:1.0]];

}

- (IBAction)activityChanged:(id)sender {
    self.currentActivity = [activityControl titleForSegmentAtIndex:activityControl.selectedSegmentIndex];
    
    NSString *message;
    if ([[activityControl titleForSegmentAtIndex:activityControl.selectedSegmentIndex
          ] isEqual:@"OffCharter"]) {
        if ([self.activeTimeLabel.text isEqualToString:@"Time Off Charter active"]) {
            message = @"";
        }
        else {
            message = @"Time Off Charter started...";
        }
        self.activeTimeLabel.text = @"Time Off Charter active";
    }
    else if ([[activityControl titleForSegmentAtIndex:activityControl.selectedSegmentIndex
               ] isEqual:@"Bug"])
    {
        if ([self.activeTimeLabel.text isEqualToString:@"Time on Bugs active"]) {
            message = @"";
        }
        else {
            message = @"Time on Bugs started...";
        }
        self.activeTimeLabel.text = @"Time on Bugs active";

    }
    else if ([[activityControl titleForSegmentAtIndex:activityControl.selectedSegmentIndex
               ] isEqual:@"Setup"])
    {
        if ([self.activeTimeLabel.text isEqualToString:@"Time on Setup active"]) {
            message = @"";
        }
        else {
            message = @"Time on Setup started...";
        }
        self.activeTimeLabel.text = @"Time on Setup active";

    }
    else if ([[activityControl titleForSegmentAtIndex:activityControl.selectedSegmentIndex] isEqual:@"Test"])
    {
        if ([self.activeTimeLabel.text isEqualToString:@"Time on Test active"]) {
            message = @"";
        }
        else { 
            message = @"Time on Test started...";
        }
        self.activeTimeLabel.text = @"Time on Test active";

    }
    
    else
    {
        message = @"";
    }
             
    self.activityChangedTip = [[CMPopTipView alloc] initWithMessage:message];
    self.activityChangedTip.delegate = self;
    self.activityChangedTip.backgroundColor = [UIColor whiteColor];
    self.activityChangedTip.textColor = [UIColor darkTextColor];
    self.activityChangedTip.textFont = [UIFont fontWithName:@"Optima" size:12.0];
    self.activityChangedTip.preferredPointDirection = PointDirectionDown;

    if (![message isEqualToString:@""]) {
        [self.activityChangedTip presentPointingAtView:self.activeTimeLabel inView:self.view animated:YES];
        [self.activityChangedTip autoDismissAnimated:YES atTimeInterval:2.0];
    }
    for (int i=0; i<[self.activityControl.subviews count]; i++)
    {
        if ([[self.activityControl.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && [[self.activityControl.subviews objectAtIndex:i]isSelected])
        {
            [[self.activityControl.subviews objectAtIndex:i] setTintColor:[UIColor darkGrayColor]];

        }
        if ([[self.activityControl.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && ![[self.activityControl.subviews objectAtIndex:i] isSelected])
        {
            [[self.activityControl.subviews objectAtIndex:i] setTintColor:[UIColor colorWithRed:77.0/256.0 green:168.0/256.0 blue:36.0/256.0 alpha:1.0]];
        }
    }
    
    for (int i=0; i<[self.nonTimeActivityControl.subviews count]; i++)
    {
        if ([[self.nonTimeActivityControl.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && [[self.nonTimeActivityControl.subviews objectAtIndex:i]isSelected])
        {
            [[self.nonTimeActivityControl.subviews objectAtIndex:i] setTintColor:[UIColor lightGrayColor]];
        }
        if ([[self.nonTimeActivityControl.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && ![[self.nonTimeActivityControl.subviews objectAtIndex:i] isSelected])
        {
            [[self.nonTimeActivityControl.subviews objectAtIndex:i] setTintColor:[UIColor lightGrayColor]];
        }
    }

    
    [self.activityControl setSelectedSegmentIndex:UISegmentedControlNoSegment];

}
@end

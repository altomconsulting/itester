//
//  testSessionHelp.m
//  Test Session
//
//  Created by Ru Cindrea on 4/20/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import "testSessionHelp.h"

@implementation testSessionHelp

-(BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void) viewDidLoad {
}

- (void)viewDidUnload {
    [self setHelpTextView:nil];
    [self setFeedbackButton:nil];
    [super viewDidUnload];
}
- (IBAction)donePressed:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
}
- (IBAction)feedback:(id)sender {
    
    NSString *emailTitle = @"iTester Feedback";
    NSArray *toRecipents = [NSArray arrayWithObject:@"itester@altom.fi"];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setToRecipients:toRecipents];
    
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }

    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end

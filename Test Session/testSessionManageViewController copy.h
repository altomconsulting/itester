//
//  testSessionManageViewController.h
//  Test Session
//
//  Created by Ru Cindrea on 3/26/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>

//#import "GTLDrive.h"

@interface testSessionManageViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate,QLPreviewControllerDataSource, QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *sessionTableView;

- (IBAction)done:(id)sender;

-(void)showActivityViewController;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *authButton;
@property (strong, nonatomic) IBOutlet UILabel *offlineView;
@property (strong, nonatomic) NSString *offlineSessions;
- (IBAction)authButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *uploadButton;
- (IBAction)upload:(id)sender;
- (IBAction)showSessions:(id)sender;
//@property GTLDriveFile *driveFile;
@property NSMutableArray *tableData;



@property (strong, nonatomic) IBOutlet UIBarButtonItem *showSessionsButton;
@end

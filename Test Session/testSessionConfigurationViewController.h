//
//  testSessionConfigurationViewController.h
//  Test Session
//
//  Created by Ru Cindrea on 3/18/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "testSessionViewController.h"
#import "testSessionManageViewController.h"
#import "testSessionHelp.h"
#import <CoreText/CoreText.h>
#import "CMPopTipView.h"




@interface testSessionConfigurationViewController : UIViewController  <UINavigationControllerDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, CMPopTipViewDelegate>

@property (strong, nonatomic) IBOutlet UISlider *sessionSlider;
@property (strong, nonatomic) IBOutlet UILabel *lengthLabel;
@property (strong, nonatomic) IBOutlet UILabel *areaLabel;

- (IBAction)help:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *environment;
@property (strong, nonatomic) IBOutlet UITextField *area;
@property (strong, nonatomic) IBOutlet UILabel *envLabel;
@property (strong, nonatomic) IBOutlet UITextField *reporter;
@property (strong, nonatomic) NSDictionary *fontAttributes;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *helpButton;

@property (strong, nonatomic) IBOutlet UIButton *infoButton;
@property (strong, nonatomic) IBOutlet UITextView *charter;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *startButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *manageButton;
@property (strong, nonatomic) IBOutlet UILabel *enterCharterLabel;

- (IBAction)saveReporter:(id)sender;

- (IBAction)sliderChanged:(id)sender;

@end

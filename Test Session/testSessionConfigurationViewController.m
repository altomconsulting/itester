//
//  testSessionConfigurationViewController.m
//  Test Session
//
//  Created by Ru Cindrea on 3/18/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import "testSessionConfigurationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import "CMPopTipView.h"


@interface testSessionConfigurationViewController ()
@property (nonatomic, strong)	NSMutableArray	*visiblePopTipViews;
@property (nonatomic, strong)	id				currentPopTipViewTarget;


@end

@implementation testSessionConfigurationViewController

-(BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)viewDidLoad
{
    [_charter.layer setBorderColor: [[UIColor lightGrayColor] CGColor]];
    [_charter.layer setBorderWidth: 0.5];
    [_charter.layer setCornerRadius:8.0f];
    [_charter.layer setMasksToBounds:YES];
	self.visiblePopTipViews = [NSMutableArray array];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _reporter.text = [defaults stringForKey:@"reporter"];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)configurePopUpTipView: (NSString*) text :(UIView*) pointAtView :(PointDirection) direction{
    
    CMPopTipView *popTipView;
    
    [popTipView dismissAnimated:YES];
    popTipView = [[CMPopTipView alloc] initWithMessage:text];
    
    popTipView.backgroundColor = [UIColor whiteColor];
    //popTipView.textColor = [UIColor colorWithRed:77.0/256.0 green:168.0/256.0 blue:36.0/256.0 alpha:1.0];
    popTipView.textColor =[UIColor blackColor];
    popTipView.borderColor = [UIColor colorWithRed:77.0/256.0 green:168.0/256.0 blue:36.0/256.0 alpha:1.0];
    popTipView.textFont = [UIFont fontWithName:@"Optima" size:13.0];
    popTipView.preferredPointDirection = direction;
    [popTipView presentPointingAtView:pointAtView inView:self.view animated:YES];
    [popTipView autoDismissAnimated:YES atTimeInterval:20.0];
    [popTipView dismissTapAnywhere];
    [self.visiblePopTipViews addObject:popTipView];
}

-(void)viewDidAppear:(BOOL)animated
{
    float length = [[[NSUserDefaults standardUserDefaults] stringForKey:@"defaultLength"] floatValue];
    [_sessionSlider setMinimumValue:5.0];
    [_sessionSlider setMaximumValue:120.0];
    [_sessionSlider setValue:length animated:YES];
    
    //shadow not in use
    NSShadow *textShadow = [[NSShadow alloc] init];
    textShadow.shadowColor = [UIColor grayColor];
    textShadow.shadowBlurRadius = 2.0;
    textShadow.shadowOffset = CGSizeMake(1,1);
    
    _startButton.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    _startButton.layer.shadowOpacity = 1.5;
    _startButton.layer.shadowRadius = 8;
    _startButton.layer.shadowOffset = CGSizeMake(10.0f, 10.0f);
    
    _fontAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Optima" size:14.0]//,
                               // NSShadowAttributeName: textShadow
                                 };
    
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Session length: %.0f mins", length] attributes:_fontAttributes];

    _lengthLabel.attributedText = string;
}


- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"gotoSession"]) {
        
        if ([self.charter.text isEqual:@""] || [self.charter.text isEqual:@"Tap here to enter charter description before starting the session..."]) {
            self.enterCharterLabel.textColor = [UIColor redColor];
            self.enterCharterLabel.text = @"Enter charter description first to get started";
            return NO;
        }
        else {
            return YES;
        }
    }
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isEqual:self.startButton]) {


        testSessionViewController* nextVC = (testSessionViewController*)segue.destinationViewController;
        [nextVC setMinutes:_sessionSlider.value];
        [nextVC setCharter:_charter.text];
        [nextVC setReporter:_reporter.text];
        [nextVC setArea:self.area.text];
        [nextVC setEnvironment:self.environment.text];
    }
//    else if ([sender isEqual:self.manageButton]) {
//        testSessionManageViewController* nextVC = (testSessionManageViewController*)segue.destinationViewController;
  //      // Show contents of Documents directory
    //    NSError *error;
      //  NSFileManager *fileMgr = [NSFileManager defaultManager];
        //NSString *documentsDirectory = [NSHomeDirectory()
          //                              stringByAppendingPathComponent:@"Documents"];
        //NSString *offlineFiles = [NSString stringWithFormat: @"Documents directory: %@",[fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]];
 //   }
    
}

- (IBAction)saveReporter:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject:_reporter.text forKey:@"reporter"];
}

- (IBAction)sliderChanged:(id)sender {
    
    [_sessionSlider setValue:((int)((_sessionSlider.value + 2.5) / 5) * 5) animated:NO];
    
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Session length: %i mins", (int)_sessionSlider.value] attributes:_fontAttributes];    
    _lengthLabel.attributedText = string;

    
}


- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView {
    [self.visiblePopTipViews removeObject:popTipView];
    self.currentPopTipViewTarget = nil;
}


- (void)dismissAllPopTipViews {
	while ([self.visiblePopTipViews count] > 0) {
		CMPopTipView *popTipView = [self.visiblePopTipViews objectAtIndex:0];
		[popTipView dismissAnimated:YES];
		[self.visiblePopTipViews removeObjectAtIndex:0];
}
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
    
    if ([self.charter.text isEqual:@""] || [self.charter.text isEqual:@"Tap here to enter charter description before starting the session..."]) {
        self.enterCharterLabel.textColor = [UIColor redColor];
    }
    else {
        self.enterCharterLabel.text = @"";
   }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqual:@"Tap here to enter charter description before starting the session..."]){
        [textView setText:@""];
    }
}

- (void)viewDidUnload {
    [self setReporter:nil];
    [self setStartButton:nil];
    [self setManageButton:nil];
    [self setEnterCharterLabel:nil];
    [self setHelpButton:nil];
    [self setEnvLabel:nil];
    [self setAreaLabel:nil];
    [self setEnvironment:nil];
    [self setArea:nil];
    [self setInfoButton:nil];
    [super viewDidUnload];
}


- (IBAction)help:(id)sender {
    
    CMPopTipView *popTipView;

    
    if ([self.helpButton.title isEqual:@"Help"]){
        
        [self dismissAllPopTipViews];

        [self configurePopUpTipView:@"iTester helps you take notes during your exploratory testing sessions and allows you to collect relevant metrics to be used in Session Based Test Management.\n\nUse this info button to learn more about SBTM and session based testing and to find out how to use your iTester session to create a Test Summary Report." :self.infoButton :PointDirectionDown];
        
        self.currentPopTipViewTarget = sender;
        [self.helpButton setTitle:@"More 2/7"];
        
    }
    
    
    else if ([self.helpButton.title isEqual:@"More 2/7"]){
        [self dismissAllPopTipViews];
      
        [popTipView dismissAnimated:YES];
        
        [self configurePopUpTipView:@"Start by entering a test charter - this is the goal or the agenda for your test session.\n\nA good charter will offer you direction and suggest ideas but will not restrict you by being too specific \nor confuse you by being too broad." :self.charter :PointDirectionUp];
		self.currentPopTipViewTarget = sender;

        [self.helpButton setTitle:@"More 3/7"];
        
    }
    else if ([self.helpButton.title isEqual:@"More 3/7"]){
        [self dismissAllPopTipViews];
        
        [popTipView dismissAnimated:YES];
        
        [self configurePopUpTipView:@"Enter your own email address here to easily email the session file to yourself once you have finished \ntesting." :self.reporter :PointDirectionUp];
		self.currentPopTipViewTarget = sender;

        [self.helpButton setTitle:@"More 4/7"];
        
    }
    else if ([self.helpButton.title isEqual:@"More 4/7"]){
        [self dismissAllPopTipViews];
        [self configurePopUpTipView:@"Enter any relevant information regarding the area or feature that you are covering with this charter.\n\nThis info can be used later on as part of your SBTM Summary Report." :self.area :PointDirectionDown];
        self.currentPopTipViewTarget = sender;

        [self.helpButton setTitle:@"More 5/7"];
        
    }
    else if ([self.helpButton.title isEqual:@"More 5/7"]){
        [self dismissAllPopTipViews];
        
        [self configurePopUpTipView:@"Use the slider to adjust the length of your session." :self.sessionSlider :PointDirectionUp];
		self.currentPopTipViewTarget = sender;

        [self.helpButton setTitle:@"More 6/7"];
        
    }
    else if ([self.helpButton.title isEqual:@"More 6/7"]){
        [self dismissAllPopTipViews];
        
        [self configurePopUpTipView:@"Use this to enter any environment information that you might find useful.\n\nFor example, you might have: 'Safari, iOS6.1, iPhone5, Build 10.15'." :self.environment :PointDirectionUp];
		self.currentPopTipViewTarget = sender;

        [self.helpButton setTitle:@"More 7/7"];
        //[popTipView autoDismissAnimated:YES atTimeInterval:20.0];
        
    }
    else if ([self.helpButton.title isEqual:@"More 7/7"]){
        [self dismissAllPopTipViews];
        
        
        [self configurePopUpTipView:@"Start the session whenever you are ready.\n\nOnce you have finished the session and you return to this view, use the Organize button on the top to manage your sessions locally or \nview and open them \nusing your favorite app." :self.startButton :PointDirectionDown];
        
		self.currentPopTipViewTarget = sender;

        [self.helpButton setTitle:@"Help"];
    }

    
}
@end

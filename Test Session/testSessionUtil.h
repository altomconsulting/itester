//
//  testSessionUtil.h
//  Test Session
//
//  Created by Ru Cindrea on 4/2/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface testSessionUtil : NSObject

+ (UIAlertView *)showLoadingMessageWithTitle:(NSString *)title
                                    delegate:(id)delegate;
+ (void)showErrorMessageWithTitle:(NSString *)title
                          message:(NSString *)message
                         delegate:(id)delegate;
+ (void)showInfoMessageWithTitle:(NSString *)title
                          message:(NSString *)message
                         delegate:(id)delegate;

+ (UIAlertView *)showLogoutConfirmation:(NSString *)title
                      delegate:(id)delegate;

@end

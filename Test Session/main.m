//
//  main.m
//  Test Session
//
//  Created by Ru Cindrea on 3/18/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "testSessionAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([testSessionAppDelegate class]));
    }
}

//
//  VATestObserver.m
//  Test Session
//
//  Created by Ru Cindrea on 10/3/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

#import "VATestObserver.h"

static id mainSuite = nil;

@implementation VATestObserver

+ (void)initialize {
    [[NSUserDefaults standardUserDefaults] setValue:@"VATestObserver" forKey:SenTestObserverClassKey];
    
    [super initialize];
}

+ (void)testSuiteDidStart:(NSNotification*)notification {
    [super testSuiteDidStart:notification];
    
    SenTestSuiteRun* suite = notification.object;
    
    if (mainSuite == nil) {
        mainSuite = suite;
    }
}

+ (void)testSuiteDidStop:(NSNotification*)notification {
    [super testSuiteDidStop:notification];
    
    SenTestSuiteRun* suite = notification.object;
    
    if (mainSuite == suite) {
        UIApplication* application = [UIApplication sharedApplication];
        [application.delegate applicationWillTerminate:application];
    }
}
@end

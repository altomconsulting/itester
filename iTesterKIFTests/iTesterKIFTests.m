//
//  iTesterKIFTests.m
//  Test Session
//
//  Created by Ru Cindrea on 10/2/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

//#import <SenTestingKit/SenTestingKit.h>
#import <KIF/Kif.h>

@interface iTesterKIFTests : KIFTestCase

@end

@implementation iTesterKIFTests


- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testInfoPageNoFeedback
{
    [tester tapViewWithAccessibilityLabel:@"infoButton"];
    [tester waitForViewWithAccessibilityLabel:@"infoTextView"];
    
    [tester waitForViewWithAccessibilityLabel:@"Feedback"];
    [tester waitForViewWithAccessibilityLabel:@"Stop"];
    [tester tapViewWithAccessibilityLabel:@"Stop"];
}


- (void)testInfoPageFeedbackEmail
{
//    [tester waitForViewWithAccessibilityLabel:@"infoButton"];
//    [tester tapViewWithAccessibilityLabel:@"infoButton"];
    
//    [tester tapViewWithAccessibilityLabel:@"Feedback"];
//    [tester waitForTimeInterval:10];
    //[tester tapScreenAtPoint:CGPointMake(10.0f, 10.0f)];
}

-(void)testStartSession
{
    
    //[tester waitForViewWithAccessibilityLabel:@"infoTextView"];
    [tester enterText:@"Some new charter" intoViewWithAccessibilityLabel:@"charterTextView"];
    
    
    [tester enterText:@"environment details" intoViewWithAccessibilityLabel:@"environment"];
    [tester enterText:@"area" intoViewWithAccessibilityLabel:@"area"];
    [tester tapViewWithAccessibilityLabel:@"startButton"];
    
    [tester waitForViewWithAccessibilityLabel:@"activityTextView"];

}


-(void)testManageLocalSessions
{
    [tester waitForViewWithAccessibilityLabel:@"Organize"];
    [tester tapViewWithAccessibilityLabel:@"Organize"];
    
    [tester waitForViewWithAccessibilityLabel:@"manageView"];
    
    [tester waitForViewWithAccessibilityLabel:@"Stop"];
    [tester waitForViewWithAccessibilityLabel:@"Local Sessions"];
    
    [tester tapRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] inTableViewWithAccessibilityIdentifier:@"localSessionsTable"];
    [tester waitForViewWithAccessibilityLabel:@"Done"];
    [tester tapViewWithAccessibilityLabel:@"Done"];
    [tester tapViewWithAccessibilityLabel:@"Stop"];
    
}
@end



//
//  iTester_KIFTests.m
//  iTester KIFTests
//
//  Created by Ru Cindrea on 9/25/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//


//#import <SenTestingKit/SenTestingKit.h>
#import <KIF/KIF.h>
//#import "KIFUITestActor+EXAdditions.h"

@interface iTester_KIFTests : KIFTestCase

@end

@implementation iTester_KIFTests

- (void)setUp
{
    [super setUp];
    [tester waitForTimeInterval:2];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    [tester enterText:@"some env" intoViewWithAccessibilityLabel:@"environment"];
    [tester enterText:@"New charter" intoViewWithAccessibilityLabel:@"charterTextView"];
    
    [tester tapViewWithAccessibilityLabel:@"startButton"];
}

@end

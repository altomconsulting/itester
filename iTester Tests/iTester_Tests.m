//
//  iTester_Tests.m
//  iTester Tests
//
//  Created by Ru Cindrea on 9/25/13.
//  Copyright (c) 2013 Ru Cindrea. All rights reserved.
//

//#import <XCTest/XCTest.h>
#import <KIF/KIF.h>

@interface iTester_Tests : KIFTestCase

@end

@implementation iTester_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSomething
{
    //[tester enterText:@"user@example.com" intoViewWithAccessibilityLabel:@"Login User Name"];
    //[tester enterText:@"thisismypassword" intoViewWithAccessibilityLabel:@"Login Password"];
    //[tester tapViewWithAccessibilityLabel:@"Log In"];
    
    [tester enterText:@"some env" intoViewWithAccessibilityLabel:@"environment"];
}

//- (void)testExample
//{
  //  XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
//}

@end
